# adidas Challenge - Julian Barragan
## adidas

A cart page for adidas.com/us

### Build With
- React + Redux
- Sass
- Bootstrap
- Lodash
- Express
- Jest
- Enzyme


### Getting Started
Install dependencies
```shell
 $ npm i
```

Run proxy middleware
```shell
 $ npm run start-express
```
Run in another tab the application
```shell
 $ npm start
```


