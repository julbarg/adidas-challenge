import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import axios from '../../config/axiosAdidas';
import Product from './Product';
import { connect } from 'react-redux';
import { createBasket } from '../../redux/actions/basketAction';

export const MyContext = React.createContext();

const ProductPage = ({ basketId, history, dispatch }) => {

    const [ activeProduct, setActiveProduct] = useState({});

    useEffect(() => {
        async function getRandomProduct () {
			const { data: dataProducts } = await axios.get('/api/search/tf/taxonomy?query=men-running-shoes');
            const items = dataProducts.itemList.items;
            const productIds = items
                .filter(item => (item.previewTo && Date.parse(item.previewTo) < new Date()))
                .map(item => item.productId);
            const productId = Math.floor(Math.random() * productIds.length);

            const { data: dataProduct} = await axios.get(`/api/products/${productIds[productId]}`);

            const { data: dataProductAvailability} = await axios.get(`/api/products/${productIds[productId]}/availability`);

            const sku = getSku(dataProductAvailability);

            return getProduct(dataProduct, sku);
		}

		if (_.isEmpty(basketId)) {
			axios.post('/api/checkout/baskets')
				.then(data => {
					dispatch(createBasket(data.basketId));
				})
				.catch(err => {
					console.log(err);
				});
		}
        getRandomProduct()
			.then(data => setActiveProduct(data))
			.catch(err => console.error(err));
        ;
    }, [basketId, dispatch]);

    const getSku = ({ variation_list  = [] }) => {
        return variation_list
            .filter(variation => variation.availability_status === 'IN_STOCK')
            .map(({ sku, size }) => {
                return {
                    size,
                    sku
                }
            });
    };

    const getProduct = (dataProduct, sku) => {
        return {
            brand: dataProduct.attribute_list.brand,
            description: dataProduct.product_description.subtitle,
            imgUrl: dataProduct.view_list,
            name: dataProduct.name,
            price: dataProduct.pricing_information.standard_price,
            sku
        }
    };

    const renderProduct = () => {
        return !_.isEmpty(activeProduct)
            ? <Product product={activeProduct} />
            : null
    };

    return (
        <MyContext.Provider value={{history}}>
			{renderProduct()}
        </MyContext.Provider>
    );
}

const mapStateToProps = ({basketId}) => {
	return {
		basketId
	};
};

export default connect(
	mapStateToProps
)(ProductPage);