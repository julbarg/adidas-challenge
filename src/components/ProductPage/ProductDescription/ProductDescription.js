import _ from 'lodash';
import axios from '../../../config/axiosAdidas';
import React, { useContext,useRef, useState } from 'react';
import { addItem } from '../../../redux/actions/basketAction';
import { connect } from 'react-redux';
import { getToken } from '../../../utils';
import { MyContext } from '../ProductPage';
import PropTypes from 'prop-types';

const ProductDescription = ({ basketId, product, dispatch }) => {
	const [message, setMessage] = useState('');
    const sizeRef = useRef();
    const quantityRef = useRef();
    const context = useContext(MyContext);

    const renderOption = ({ sku, size }) => (
        <option
            key={sku}
            value={sku}
        >
            {size}
        </option>
    );

    const renderCounter = () => {
        const nums = _.range(1, 15);

        return (
            <select ref={quantityRef}>
                {nums.map(num => (
                    <option
                        key={num}
                        value={num}
                    >
                        {num}
                    </option>
                ))}
            </select>
        )
	};

	const renderSizing = () => (
		<select ref={sizeRef} onChange={() => {setMessage('')}}>
			<option
				value='none'
			>
				SELECT SIZE
			</option>
			{product.sku.map(renderOption)}
		</select>
	);

	const renderProductDescription = () => (
		<React.Fragment>
			<div className='Product_Description_Subtitle'>
                {product.brand}
            </div>
            <div className='Product_Description_Title'>
                {product.name}
            </div>
            <div className='Product_Description_Price'>
                ${product.price}.00
            </div>
            <div className='Product_Description_Content'>
                {product.description}
            </div>
		</React.Fragment>
	);

	const renderButton = () => (
		<div>
			<button onClick={addToBag}>
				ADD TO BAG
			</button>
		</div>
	);

	const renderMessage = () => (
		<p>{message}</p>
	);

    const addToBag = () => {
		const sku = sizeRef.current.value;
		const quantityValue = quantityRef.current.value;

		if (sku === 'none') {
			setMessage('Please select one size');
		} else {
			const config = {
				headers: {
					Authorization: `Bearer ${getToken()}`
				}
			};

			const body = [{
				"productId": sku,
				"quantity": parseInt(quantityValue)
			}];

			axios.post(
				`/api/checkout/baskets/${basketId}/items`,
				body,
				config
			)
			.then(console.log)
			.catch(console.error);

			const item = {
                imgUrl: product.imgUrl[0].image_url,
				name: product.name,
				price: product.price,
				quantity: quantityValue,
                size: product.sku.find(item => item.sku === sku).size,
				sku
			};

			dispatch(addItem(item));
			context.history.push('/bag');
		}
    };

    return (
        <div className='Product_Description col-md-6 col-lg-4'>
            {renderProductDescription()}
			<div className='d-flex'>
				{renderSizing()}
				{renderCounter()}
			</div>
			{renderMessage()}
			{renderButton()}
        </div>
    );
}

const mapStateToProps = (state) => {
	return {
		basketId: state.basketId
	}
};

ProductDescription.propTypes = {
	basketId: PropTypes.string,
	product: PropTypes.shape({
		brand: PropTypes.string,
        description: PropTypes.string,
        imgUrl: PropTypes.array,
		price: PropTypes.number,
		sku: PropTypes.arrayOf(
			PropTypes.shape({
				size: PropTypes.string,
				sku: PropTypes.sku
			})
		)
	})
}

export default connect(mapStateToProps)(ProductDescription);