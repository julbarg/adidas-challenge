import './Product.scss';
import ProductCarrousel from '../ProductCarrousel';
import ProductDescription from '../ProductDescription';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';

const Product = ({ product }) => {
    const [activeIndex, setActiveIndex] = useState(0);
    const [imageActive, setImageActive] = useState({});

    useEffect(() => {
        setImageActive(product.imgUrl[activeIndex])
    }, [activeIndex, product]);

    const forward = () => {
		if(activeIndex > 0) {
			setActiveIndex(activeIndex - 1);
		} else {
			setActiveIndex(product.imgUrl.length - 1);
		}
	};

	const back = () => {
		if (activeIndex < product.imgUrl.length - 1) {
			setActiveIndex(activeIndex + 1);
		} else {
			setActiveIndex(0);
		}
	};

    return (
        <div className='Product'>
            <div className="row">
                <ProductCarrousel
					className='col-md-6 col-lg-8'
					onFoward={forward}
                    onBack={back}
                    index={activeIndex}
                    image={imageActive}
                />
                <ProductDescription
					className='col-md-6 col-lg-4'
					product={product}/>
            </div>

        </div>
    );
}

Product.propTypes = {
    brand: PropTypes.string,
    description: PropTypes.string,
    imgUrl: PropTypes.array,
    price: PropTypes.number,
    sku: PropTypes.arrayOf(
        PropTypes.shape({
            size: PropTypes.string,
            sku: PropTypes.sku
        })
    )
}

export default Product;