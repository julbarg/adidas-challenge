import Bag from './Bag';
import React from 'react';

const BagPage = () => {
    return (
        <div>
            <Bag />
        </div>
    );
}

export default BagPage;