import _ from 'lodash';
import React from 'react';
import payPalImg from '../../../../payPal.png';
import PropTypes from 'prop-types';

const BagItems = ({ items, onRemove }) => {
	const renderCounter = (quantity) => {
        const nums = _.range(1, 16);

        return (
            <select defaultValue={quantity}>
                {nums.map(num => (
                    <option
                        key={num}
						value={num}
                    >
                        {num}
                    </option>
				))}
            </select>
        )
	};

	const renderItemImage = (item) => (
		<div className='col-3 px-0'>
			<img src={item.imgUrl} alt={item.sku} className='Bag_Item_Image' />
		</div>
	);

	const remove = (prodcutId) => {
		onRemove(prodcutId);
	};

	const renderItemContent = (item) => (
		<div className='col-9 p-2 p-4'>
			<div className='d-flex justify-content-between'>
				<div>
					<div className='Bag_Item_Name pb-1'>{item.name}</div>
					<p className='mb-0'>Size: {item.size}</p>
					<p className='mb-2'>IN STOCK</p>
					{renderCounter(item.quantity)}
				</div>
				<div className='pr-4 Bag_Item_Price'>
					$ {item.price * item.quantity}.00
				</div>
			</div>
			<div className='Bag_Item_Remove'>
				<button onClick={() => remove(item.sku)}>x</button>
			</div>
		</div>
	);

	const renderItem = (item) => (
		<div key={item.sku} className='Bag_Item'>
			<div className='row'>
				{renderItemImage(item)}
				{renderItemContent(item)}
			</div>
		</div>
	);

	const renderButtons = () => (
		<div className='d-flex justify-content-between align-items-center'>
			<button className="Bag_Items_Button Bag_Items_Button_Checkout">CHECKOUT</button>
			<div>OR</div>
			<button className="Bag_Items_Button Bag_Items_Button_Paypal">
				<img src={payPalImg} alt='paypal' style={{
					width: '80px',
					padding: '3px'
				}}/>
			</button>
		</div>
	);

	return (
		<div className='Bag_Items col-md-6 col-lg-8 pl-0 py-5'>
			{items.map(renderItem)}
			{renderButtons()}
		</div>
	);
}

BagItems.propTypes = {
	items: PropTypes.arrayOf(
		PropTypes.shape({
			imgUrl: PropTypes.string,
			name: PropTypes.string,
			price: PropTypes.number,
			quantity: PropTypes.string,
			size: PropTypes.string,
			sku: PropTypes.string
		})
	),
	onRemove: PropTypes.func.isRequired
}

export default BagItems;