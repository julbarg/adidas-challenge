import React from 'react';
import payPalImg from '../../../../payPal.png';
import PropTypes from 'prop-types';

const BagCheckout = ({ items }) => {
	let price = 0;

	const renderBotones = () => (
		<React.Fragment>
			<button className="Bag_Items_Button Bag_Items_Button_Checkout mb-2">CHECKOUT</button>
			<div className='mx-auto'>OR</div>
			<button className="Bag_Items_Button Bag_Items_Button_Paypal mt-2">
				<img src={payPalImg} alt='paypal' style={{
					width: '80px',
					padding: '3px'
				}}/>
			</button>
		</React.Fragment>
	);

	const renderLabelItems = () => {
		const noItems = items.reduce((acumulator, item) => (
			acumulator + parseInt(item.quantity)
		), 0);

		const label = noItems > 1 ? 'ITEMS' : 'ITEM';
		const labelItems = `${noItems} ${label}`;

		return (
			<div>
				{labelItems}
			</div>
		);
	};

	const renderPrice = () => {
		price = items.reduce((acumulator, item) => (
			acumulator + (item.price * parseInt(item.quantity))
		), 0);

		return (
			<div>
				${price}.00
			</div>
		);
	};

	const renderDelivery = () => (
		<div className='d-flex justify-content-between my-2'>
			<div>DELIVERY</div>
			<div>FREE</div>
		</div>
	);

	const renderTotal = () => (
		<div className='font-weight-bold d-flex justify-content-between my-2'>
			<div>TOTAL</div>
			<div>{price}.00</div>
		</div>
	);

	const renderOrderSummary = () => (
		<div className='Bag_Checkout_Summary mt-5 p-2'>
			<div className='Bag_Item_Name'>Order Summary</div>
			<div className='d-flex justify-content-between my-2'>
				{renderLabelItems()}
				{renderPrice()}
			</div>
			{renderDelivery()}
			<div>SALES TAXES</div>
			{renderTotal()}
		</div>
	);

	return (
		<div className='Bag_Checkout col-4 mt-5 d-flex flex-column'>
			{renderBotones()}
			{renderOrderSummary()}
		</div>
	);
}

BagCheckout.propTypes = {
	items: PropTypes.arrayOf(
		PropTypes.shape({
			imgUrl: PropTypes.string,
			name: PropTypes.string,
			price: PropTypes.number,
			quantity: PropTypes.string,
			size: PropTypes.string,
			sku: PropTypes.string
		})
	)
}

export default BagCheckout;