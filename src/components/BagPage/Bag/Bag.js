import './Bag.scss';
import axios from '../../../config/axiosAdidas';
import BagCheckout from './BagCheckout';
import BagItems from './BagItems';
import React from 'react';
import { connect } from 'react-redux';
import { getToken } from '../../../utils';
import { removeItem } from '../../../redux/actions/basketAction';

const Bag = ({ basketId, items, dispatch }) => {
	const price = items.reduce((acumulator, item) => (
		acumulator + (item.price * item.quantity)
	), 0);

	const noItems = items.reduce((acumulator, item) => (
		acumulator + parseInt(item.quantity)
	),0);

	const labelItem = noItems > 1 ? 'items' : 'item';

	const remove = (prodcutId) => {
		let response;
		const config = {
			headers: {
				Authorization: `Bearer ${getToken()}`
			}
		};

		dispatch(removeItem(prodcutId));

		axios.delete(`/api/checkout/baskets/${basketId}/items/${prodcutId}`, config)
			.then(data => {
				response = data;
				console.log(response);
			})
			.catch(err => console.error(err));
	};

	const renderTitle = () => (
		<React.Fragment>
			<div className='Bag_Title px-5'>YOUR BAG</div>
			<div className='px-5 pt-2 Bag_Total'>
				TOTAL: ({noItems} {labelItem})
				<span className='font-weight-bold'> ${price}</span>
			</div>
		</React.Fragment>
	);

	const renderContent = () => (
		<div className='row px-5'>
			<BagItems items={items} onRemove={remove}/>
			<BagCheckout items={items} />
		</div>
	);

	return (
		<div className='Bag container p-5'>
			{renderTitle()}
			{renderContent()}
		</div>
	);
}

const mapStateToProps = ({ items, basketId }) => {
	return {
		basketId,
		items
	};
};

export default connect(mapStateToProps)(Bag);