import Adapter from 'enzyme-adapter-react-16';
import ProductDescription from '../components/ProductPage/ProductDescription';
import Enzyme from 'enzyme';
import React from 'react';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';

const mockStore = configureMockStore([]);
const storeStateMock = {
    myReducer:{
      basketId: '6569b4e7126b8558ee893a9e16'
    }
  };

Enzyme.configure({
    adapter: new Adapter()
});

describe('Test component <ProductDescription />', () => {
    let wrapper;
    let mockProps = {
        basketId: '6569b4e7126b8558ee893a9e16',
        product: {
            brand: 'Performance',
            description: 'Touch down with control, ride with comfort.',
            name: 'Ultraboost 20 Shoes',
            price: 180,
            sku: [{
                size: '7',
                sku: 'FV8451_590'
            }]
        }
    };

    let store;

    beforeEach(() => {
        store = mockStore(storeStateMock);
        wrapper = mount(
            <ProductDescription store={store} {...mockProps} />
        );
    });

    test('validate nodes', () => {
        expect(wrapper.find('.Product_Description_Subtitle').text()).toBe('Performance');
        expect(wrapper.find('.Product_Description_Title').text()).toBe('Ultraboost 20 Shoes');
        expect(wrapper.find('.Product_Description_Price').text()).toMatch('180');
    });
})

