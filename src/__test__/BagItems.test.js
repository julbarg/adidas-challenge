import Adapter from 'enzyme-adapter-react-16';
import BagItems from '../components/BagPage/Bag/BagItems';
import Enzyme from 'enzyme';
import React from 'react';
import { shallow } from 'enzyme';

Enzyme.configure({
    adapter: new Adapter()
});

describe('Test component <BagItems />', () => {
    let wrapper;
    let mockProps = {
        items: [
            {
                imgUrl: 'https://assets.adidas.com/images/w_600,f_auto,q_auto/aacddda695cd4ca98717ab0b011a7cbe_9366/Ultraboost_DNA_Shoes_Black_FW4324_01_standard.jpg',
                name: 'Ultraboost DNA Shoes',
                price: 180,
                quantity: '1',
                size: '11',
                sku: "FW4324_670"
            }
        ],
        onRemove: () => { }
    };

    beforeEach(() => {
        wrapper = shallow(<BagItems {...mockProps} />);
    });

    test('validate nodes', () => {
        expect(wrapper.find('.Bag_Items').exists()).toBe(true);
        expect(wrapper.find('.Bag_Item')).toHaveLength(mockProps.items.length);
        expect(wrapper.find('img.Bag_Item_Image')
            .prop('src'))
            .toBe('https://assets.adidas.com/images/w_600,f_auto,q_auto/aacddda695cd4ca98717ab0b011a7cbe_9366/Ultraboost_DNA_Shoes_Black_FW4324_01_standard.jpg');
        expect(wrapper.find('.Bag_Item_Name').text()).toBe('Ultraboost DNA Shoes');
        expect(wrapper.find('.Bag_Item_Price').text()).toMatch('180');
    });
})

