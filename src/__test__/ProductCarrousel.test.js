import Adapter from 'enzyme-adapter-react-16';
import ProductCarrousel from '../components/ProductPage/ProductCarrousel';
import Enzyme from 'enzyme';
import React from 'react';
import { shallow } from 'enzyme';

Enzyme.configure({
    adapter: new Adapter()
});

describe('Test component <ProductCarrousel />', () => {
    let wrapper;
    let mockProps = {
        image: {
            image_url: 'https://assets.adidas.com/images/w_600,f_auto,q_auto/aacddda695cd4ca98717ab0b011a7cbe_9366/Ultraboost_DNA_Shoes_Black_FW4324_01_standard.jpg',
        }
    };

    beforeEach(() => {
        wrapper = shallow(<ProductCarrousel {...mockProps} />);
    });

    test('validate nodes', () => {
        expect(wrapper.find('img')
            .prop('src'))
            .toBe('https://assets.adidas.com/images/w_600,f_auto,q_auto/aacddda695cd4ca98717ab0b011a7cbe_9366/Ultraboost_DNA_Shoes_Black_FW4324_01_standard.jpg');
    });
})

