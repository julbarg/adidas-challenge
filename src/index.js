import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import App from './App';
import React from 'react';
import ReactDOM from 'react-dom';
import {store, persistor }from './redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

ReactDOM.render(
    <Provider store={store}>
		<PersistGate persistor={persistor}>
			<App />
		</PersistGate>
  	</Provider>,
    document.getElementById('root')
);
