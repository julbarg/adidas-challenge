export const CREATE_BASKET = 'CREATE_BASKET';
export const ADD_ITEM = 'ADD_TIME';
export const REMOVE_ITEM = 'REMOVE_ITEM';

export const createBasket = (basketId) => {
	return {
		payload: {
			basketId
		},
		type: CREATE_BASKET
	};
};

export const addItem = (item) => {
	return {
		payload: {
			item
		},
		type: ADD_ITEM
	}
};

export const removeItem = (productId) => {
	return {
		payload: {
			productId
		},
		type: REMOVE_ITEM
	}
};