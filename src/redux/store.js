import basketReducer from './reducers/basketReducer';
import thunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';

const composeEnhancers = composeWithDevTools({
	name: 'Redux',
	realtime: true,
	trace: true,
	traceLimit: 20
});

const persistConfig = {
	key: 'root',
	storage,
}

const persistedReducer = persistReducer(persistConfig, basketReducer)

let store = createStore(
	persistedReducer,
	composeEnhancers(
		applyMiddleware(thunk)
	)
);
let persistor = persistStore(store);

export { store, persistor }