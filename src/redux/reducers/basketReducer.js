import { ADD_ITEM, CREATE_BASKET, REMOVE_ITEM } from '../actions/basketAction';

const initialState = {
	basketId: '435c1f89fa90460a5670da82a6',
	items: [
		{
            imgUrl:
            'https://assets.adidas.com/images/w_600,f_auto,q_auto/f534484f331648c08e44aaea00f38567_9366/Ultraboost_20_Shoes_Blue_FV8450_01_standard.jpg',
			name: 'Ultraboost 20 Shoes',
			price: 180,
			quantity: '1',
			size: '8.5',
			sku: 'FV8450_620'
		},
		{
            imgUrl:
            'https://assets.adidas.com/images/w_600,f_auto,q_auto/ebbf766770f04e6e8474ab2001606d6a_9366/Ultraboost_20_Shoes_Black_FV0033_01_standard.jpg',
			name: 'Ultraboost 20 Shoes',
			price: 180,
			quantity: '1',
            size: '11',
			sku: 'FV0033_670'
		}
	]
};

function basket(state = initialState, action) {
	switch (action.type) {
		case CREATE_BASKET:
			return {
				...state,
				basketId: action.payload.basketId
			};

		case ADD_ITEM:
			return {
				...state,
				items: [...state.items, action.payload.item]
			};

		case REMOVE_ITEM:
			return {
				...state,
				items: state.items.filter(
					item => item.sku !== action.payload.productId
				)
			};

		default:
			return state;
	}
};

export default basket;
