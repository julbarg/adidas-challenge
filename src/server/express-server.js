const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
var cors = require('cors');

/**
 * Configure proxy middleware
 */
const jsonPlaceholderProxy = createProxyMiddleware({
  target: 'https://www.adidas.com',
  changeOrigin: true,
  logLevel: 'debug'
});

const app = express();

app.use('/api', cors(), jsonPlaceholderProxy);

app.listen(4000);
